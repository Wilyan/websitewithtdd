from django.shortcuts import render, redirect
from .forms import Status_Form, SubsrciptionForm
from .models import Status, Subsrciption
import requests
from django.http import JsonResponse, HttpResponse
import json

# Create your views here.
response = {}
def landingPage(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('/website/landing-page/')
    else:
        allStatus = Status.objects.all()
        return render(request, 'landing-page.html', {'allStatus': allStatus, 'form' : form})

def profil(request):
    return render(request, 'profil.html', {})

def books(request):
    return render(request, 'books.html', {})

def data(request, search=None):
    url = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + str(search))
    dataJson = url.json()
    return JsonResponse(dataJson)

def subscription(request):
    form = SubsrciptionForm()
    return render(request, "subscription.html", {'form': form})

def subscriptionPost(request):
    if request.method ==  "POST":
        form = SubsrciptionForm(request.POST)
        response_data = {}
        if(form.is_valid()):
            form.save()
            response_data['result'] = 'Create post successful!'
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
            )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

def validateEmail(request):
    email = request.POST.get('email', None)
    data = {'is_taken': Subsrciption.objects.filter(email=email).exists()}
    return JsonResponse(data)

def subscriberList(request):
    data = Subsrciption.objects.all()
    subcriber_list = [{'name' : obj.name, 'email' : obj.email} for obj in data]
    return JsonResponse(subcriber_list, safe=False)

def subscriber(request):
    return render(request, 'subscriber.html', {})

def deleteSubscriber(request, email=None):
    Subsrciption.objects.filter(email=str(email)).delete()
    return HttpResponse("OK")
