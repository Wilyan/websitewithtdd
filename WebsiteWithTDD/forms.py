from django import forms
from .models import Status, Subsrciption

# Create your models here.
class Status_Form(forms.ModelForm):
    error_messages = {
        'required': 'This field is required.',
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan judul...'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 6,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan deskripsi...'
    }

    title = forms.CharField(label='', required=True, max_length=27, 
    widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True,
    widget=forms.Textarea(attrs=description_attrs))

    class Meta:
        model = Status
        fields = ('title', 'description')

class SubsrciptionForm(forms.ModelForm):
    name_attrs = {
        'class': 'form-subsciption',
        'placeholder':'Full name'
    }
    email_attrs = {
        'class': 'form-subsciption',
        'placeholder':'Email'
    }
    password_attrs = {
        'class': 'form-subsciption',
        'placeholder':'Password'
    }

    name = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='', required=True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='', required=True, min_length=8, widget=forms.PasswordInput(attrs=password_attrs))
    
    class Meta:
        model = Subsrciption
        fields = ('name', 'email', 'password')
