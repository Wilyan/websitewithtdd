$('#buttonSwitch').click(function() {
    if ($(this).is(':checked')) {
        $('#cont1').css({
        'color': '#3875d7',
        'font-family': "'Oxygen', serif"
        });
    }
    else {
        $('#cont1').css({
        'color': 'black',
        'font-family': "'Times New Roman', Times, serif"
        });
    }
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
   
$(function() {
  $(".loader").fadeOut(2000, function(){
    $(".mainContent").fadeIn(1000);
  });
});

var favorite=0;
function  fav(id){
	var ids= $('#' +id);
	var srcs= ids[0].src;
	 if (srcs.includes('nonfavorite')) {
        favorite++;
        ids[0].src = '../static/favorite.png';
    } else {
        favorite--;
        ids[0].src = '../static/nonfavorite.png';
    }
    $('#favorite').html('<h2>Total My Favorite Books : '+ favorite + '</h2>');
}

function  selectedCategory(category){
  $.ajax({
    url : '/data/'+category,
    dataType : 'json'
  }).done(function(data){
      favorite = 0;
      $('#favorite').html('<h2>Total My Favorite Books : '+ favorite + '</h2>');
      $('#booksTable').empty();
      $('#booksTable').append('<tr><th id="table">Cover</th><th id="table">Title</th><th id="table">Author</th><th>Fav</th></tr>');
      $.each(data.items, function(i, val){
        var title = data.items[i].volumeInfo.title;
        var author = data.items[i].volumeInfo.authors;
        var image = '<img id="zoom" src="' + data.items[i].volumeInfo.imageLinks.thumbnail +'">';
        $('#booksTable').append('<tr><td>' + image + '</td><td>' + title + '</td><td>' + author + '</td><td><img class="imagess" onclick=fav("'+data.items[i].id+'") id='+data.items[i].id+' src="/static/nonfavorite.png" style="height:40px;"></td></tr>');
      });
      console.log(data.items)
  });
}


$.ajax({
    url : '/data/quilting',
    dataType : 'json'
  }).done(function(data){
      $.each(data.items, function(i, val){
        var title = data.items[i].volumeInfo.title;
        var author = data.items[i].volumeInfo.authors;
        var image = '<img id="zoom" src="' + data.items[i].volumeInfo.imageLinks.thumbnail +'">';
        $('#booksTable').append('<tr><td>' + image + '</td><td>' + title + '</td><td>' + author + '</td><td><img class="imagess" onclick=fav("'+data.items[i].id+'") id='+data.items[i].id+' src="/static/nonfavorite.png" style="height:40px;"></td></tr>');
      });
  });

	function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });

  function unsubscribe(email){
    var deletedSubs = document.getElementById("table"+email+"");
    console.log(deletedSubs)
    deletedSubs.remove()
    $.ajax({
      url : 'delete-subscriber/'+ email,
    })
  }

$ (document).ready(function(){
  $('#post-form').on('submit', function(event){
      console.log("form submitted!")  // sanity check
      $.ajax({
        url : 'submit/',
        type : "POST",
        data : {
          'name': $('#id_name').val(),
          'email': $("#id_email").val(),
          'password': $("#id_password").val(),
        },
        dataType : 'json',
        success : function(json) {
          console.log(json)  // sanity check
          alert("Terimakasih sudah berlangganan" + name);
          $("#id_name").val('');
          $("#id_password").val('');
          $("#id_email").val('');
          $('#result').html(json.result);
        },
        error: function(json){
          alert('Email does not recieved');
        },
      });
      event.preventDefault();
  });

  $("#id_email").change(function () {
    console.log( $(this).val() );
    email = $(this).val();
    $("#tiny-button")[0].disabled=true;

    $.ajax({
      method: "POST",
      url: 'email-valid/',
      data: {
        'email': email
      },
      dataType: 'json',
      success: function (data) {
        console.log(data)
        if (data.is_taken) {
          console.log("HA!")  // sanity check
          alert("Email sudah terdaftar, Mohon mengganti email");
          $('#id_email').css("border-color", "red");
          $("#tiny-button")[0].disabled=true;
          $('#result').html('Change your email');
        }
        else{
          console.log("HA111!")  // sanity check
          $("#tiny-button")[0].disabled=false;
          $('#id_email').css("border-color", "white");
          $('#result').html('');
        }
      }
    });
  });

  $.ajax({
    url : 'list-subscriber',
    dataType : 'json'
  }).done(function(data){
    console.log(data)
      $.each(data, function(i, val){
        console.log(data[i])
        var name = data[i].name;
        var email = data[i].email;
        $('#subscriberTable').append('<tr id="table'+email+'"><td>' + name + '</td><td>' + email + '</td><td><button class="btn btn-primary" onclick=unsubscribe("'+email+'") id='+email+'>Unsubscribe</button></td></tr>');
      });
  });
})

