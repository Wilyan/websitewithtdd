from django.test import TestCase, Client
from django.urls import resolve
from .views import landingPage, profil, books, data
from .models import Status
from .forms import Status_Form
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class WebsiteTest(TestCase):
    def test_website_landing_page_url_is_exist(self):
        response = Client().get('/landing-page/')
        self.assertEqual(response.status_code,200)

    def test_website_profil_url_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code,200)
    
    def test_website_books_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_website_data_url_is_exist(self):
        response = Client().get('/data/quilting/')
        self.assertEqual(response.status_code,200)

    def test_website_landing_page_using_landing_page_template(self):
        response = Client().get('/landing-page/')
        self.assertTemplateUsed(response, 'landing-page.html')

    def test_website_profil_using_profil_template(self):
        response = Client().get('/profil/')
        self.assertTemplateUsed(response, 'profil.html')

    def test_website_books_using_books_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_website_landing_page_using_landingPage_func(self):
        found = resolve('/landing-page/')
        self.assertEqual(found.func, landingPage)

    def test_website_profil_using_profil_func(self):
        found = resolve('/profil/')
        self.assertEqual(found.func, profil)

    def test_website_books_using_books_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)


    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')
        # Retrieving all available activity
        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )
    
    def test_website_there_is_header(self):
        test = 'Hello apa kabar?'
        response= Client().get('/landing-page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_website_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/landing-page/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/landing-page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_website_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/landing-page/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/landing-page/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_website_there_is_content(self):
        response= profil(HttpRequest)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello my name is Mohammad Wildan Yanuar', html_response)
        self.assertIn('I am a student of Computer Science University Of Indonesia', html_response)
        self.assertIn('<img src="/static/foto.jpeg" alt="">', html_response)

class WebsiteFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(WebsiteFunctionalTest, self).setUp()
    def tearDown(self):
        self.selenium.quit()
        super(WebsiteFunctionalTest, self).tearDown()


    def test_webite_profile_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://ppw-f-mohammadwildany.herokuapp.com/')
        time.sleep(5)
        nameText = selenium.find_element_by_class_name('name').text
        footerText = selenium.find_element_by_id('bawah').text
        self.assertIn("I'M YAYAN\nINFORMATION SYSTEM STUDENT\nUNIVERSITY OF INDONESIA", nameText)
        self.assertIn('Created by Mohammad Wildan Yanuar', footerText)

    def test_webite_profile_styling(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://ppw-f-mohammadwildany.herokuapp.com/')
        time.sleep(5)
        introduction = selenium.find_element_by_class_name('introduction')
        footer = selenium.find_element_by_id('bawah')
        self.assertEquals(introduction.value_of_css_property('color') , 'rgba(245, 245, 220, 1)')
        self.assertEquals(footer.value_of_css_property('background-color') , 'rgba(14, 9, 3, 1)')





