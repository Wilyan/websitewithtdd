from django.db import models

class Status(models.Model):
    title = models.CharField(max_length=27)
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Subsrciption(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=256)
        
    def __str__(self):
        return self.name